# 欢迎使用JSsearch
## 推荐此文档和Demo配合使用，效果更佳
## 欢迎使用JSsearch2.0!
2.0更新内容：<br>
增加用偶数奇数的下标查询<br>
优化查询速度<br>
增加缓存机制，大幅提升运行速度（是1.0的N倍）<br>
JSsearch可以帮助你解决有关查询的问题 <br> (他可以帮你从海量数据中提取出你中意的那个！)<br>
它不只是单纯的查询，它可以帮助你使用你指定的关键字或者其它内容来根据内容匹配度排名！<br>
现在，就让我们一起来学习JSsearch吧！<br>
首先，再开始使用前，我们要先了解为什么要使用JSsearch！<br>
### 为什么要使用JSsearch？
第1点：快速<br>
JSsearch2.0速度非常得快，在100个以内的查询只需要0.1毫秒！在1000000（一百万）个内容中查询只需要0.3秒多！<br>
第2点：方便<br>
JSsearch2.0（js文件）只有不到8kb的内容，打开速度非常之快！而且加载也很快！<br>
第3点：实用<br>
JSsearch内置许多设置，可以帮助你完成许多不需要更改源代码即可完成的设置！<br>
第4点：效率<br>
JSsearch的效率无与伦比，2.0新加缓存机制，执行1千万条代码处理只需要0.1毫秒即可搞定！
说了这么多，心动不如行动，赶快来学习如何使用吧！<br>
### JSsearch方法
JSsearch方法没有任何参数，使用它即可获取JSsearch的版本等详细信息<br>
注意：JSsearch方法会使用return返回信息，请使用alert输出或者使用其他代码！<br>
### JSsearchSetSetting方法
JSsearchSetSetting方法可以设置你的查询设置<br>
这个方法有2个参数，分别是settingName和value，他们分别负责设置的名称和值（值只能设置为true或者false）<br>
补充：在这个版本里，settingName总共有4个，分别是searchTimer、autoRemove、autoCheck和autoCache，searchTimer可以帮助你记录查询的时间（默认关闭）【以毫秒返回】，autoRemove可以帮助你自动删除没有任何匹配度的字符串（默认开启），autoCheck可以很好的帮你检查错误（默认开启），autoCache可以帮助你自动存储缓存（默认关闭）【合理使用他们会有效的提升效率】<br>
### JSsearchByKeyWord方法
这是一个查询方法，从他的方法名称就可以看出使用关键词查询<br>
使用这个方法需要传递2个参数，分别是content和keyWord<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参<br>
### 如何看JSsearchByKeyWord方法的返回结果？
默认不调整设置的返回结果应该是这个：1,100（假设arr（content）设置为1,2、keyWord设置为1）<br>
1(也就是奇数)为后面（偶数）这个值对应的名称，后面的100是前面值和关键词的匹配度（最高100）<br>
如何看JSsearchByKeyWord方法的返回结果？(开启searchTimer)<br>
开启searchTimer后返回的数组的最后一个下标里面的值是查询用的时间，假设arr设置为1,2、keyWord设置为1，那么最后一位应该是0（在小于100个查询单位都基本是0）<br>
注意：返回类型是数组<br>
#### JSsearchByLength方法
这个方法使用长度来进行查询<br>
使用这个方法需要传递3个参数，分别是content、minLength和maxLength<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参<br>
### 如何看JSsearchByLength方法的返回结果？
默认不调整设置的返回结果应该是这个：1,true,20,true（假设content设置为1,20,300、minLength设置为1、maxLength设置为2）<br>
1(也就是奇数)为后面（偶数）这个值对应的名称，后面的true是前面值经过查询后的状态<br>
如何看JSsearchByLength方法的返回结果？(开启searchTimer)<br>
和之前的介绍方法差不多一样<br>
### JSsearchBySubscript方法
这个方法使用数组的下标来进行查询<br>
使用这个方法需要传递3个参数，分别是content、from(从哪里开始)和to(到哪里结束)<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参<br>
重要提示，如果参数含有数字，也需要加"",否则会报错<br>
### 如何看JSsearchBySubscript方法的返回结果？
【和JSsearchByLength方法的返回结果的查看方式是一样的】<br>
### JSsearchByOddNumber方法
这个方法返回提交的数组的下标为奇数的值<br>
使用这个方法只需要传递1个参数，就是content<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参<br>
### 如何看JSsearchByOddNumber方法的返回结果？
【和JSsearchByLength方法的返回结果的查看方式是一样的】<br>
### JSsearchByEvenNumber方法
这个方法返回提交的数组的下标为偶数的值<br>
使用这个方法只需要传递1个参数，就是content<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参<br>
### 如何看JSsearchByEvenNumber方法的返回结果？
【和JSsearchByLength方法的返回结果的查看方式是一样的】<br>
### JSsearchGetCache方法
这个方法可以返回你要查询的方法对应的缓存(返回的参数中间用空格分隔，第一个参数为cache，每个方法的其他参数不同)<br>
这个方法只有一个参数，就是functionName，因为不同的方法有不同的缓存池，所以需要这个参数来判断你到底是想获取哪个方法的缓存<br>
### 详解JSsearch2.0缓存机制
假设我们现在要调用查询方法：<br>
1、检查是否有缓存<br>
[2]、如果有缓存，则判断是否与请求的各项参数一样<br>
(3)、如果一样，直接返回缓存<br>
(3)、如果不一样，则继续往下执行查询代码<br>
[2]、如果没有缓存，则继续执行查询代码<br>
3、执行完成后存储缓存，等待下次继续调用<br>
注意！所有的缓存在你更改设置的时候都会被清除！<br>
<hr>
广告：Skyogo工作室官网将于2018年2月份多推出，网址：www.skyogo.com欢迎访问！