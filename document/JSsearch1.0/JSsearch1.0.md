# 欢迎使用JSsearch
## 推荐此文档和Demo配合使用，效果更佳
### JSsearch方法
JSsearch方法没有任何参数，使用它即可获取JSsearch的版本等详细信息<br>
语法：JSsearch();<br>
注意：JSsearch方法会使用return返回信息，请使用alert输出或者使用其他代码！
### JSsearchSetSetting方法
JSsearchSetSetting方法可以设置你的查询设置<br>
这个方法有2个参数，分别是settingName和value，他们分别负责设置的名称和值（值只能设置为true或者false）<br>
补充：在1.0.0版本里，settingName总共有3个，分别是searchTimer、autoRemove和autoCheck，searchTimer可以帮助你记录查询的时间（默认关闭）【以毫秒返回】，autoRemove可以帮助你自动删除没有任何匹配度的字符（默认开启），autoCheck可以很好的帮你检查错误（默认开启）【合理使用他们会有效的提升效率】
### JSsearchByKeyWord方法
这是一个查询方法，从他的方法名称就可以看出使用关键词查询<br>
使用这个方法需要传递2个参数，分别是content和keyWord<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参
### 如何看JSsearchByKeyWord方法的返回结果？
默认不调整设置的返回结果应该是这个：1,100（假设arr（content）设置为1,2、keyWord设置为1）<br>
1(也就是奇数)为后面（偶数）这个值对应的名称，后面的100是前面值和关键词的匹配度（最高100）
<br>
<br>
### 如何看JSsearchByKeyWord方法的返回结果？(开启searchTimer)<br>
开启searchTimer后返回的数组的最后一个下标里面的值是查询用的时间，假设arr设置为1,2、keyWord设置为1，那么最后一位应该是0（在小于100个查询单位都基本是0）<br>
注意：返回类型是数组
### JSsearchByLength方法
这个方法使用长度来进行查询<br>
使用这个方法需要传递3个参数，分别是content、minLength和maxLength<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参
### 如何看JSsearchByLength方法的返回结果？
默认不调整设置的返回结果应该是这个：1,true,20,true（假设content设置为1,20,300、minLength设置为1、maxLength设置为2）<br>
1(也就是奇数)为后面（偶数）这个值对应的名称，后面的true是前面值经过查询后的状态<br>
### 如何看JSsearchByLength方法的返回结果？(开启searchTimer)
和之前的介绍方法差不多一样
### JSsearchBySubscript方法
这个方法使用数组的下标来进行查询<br>
使用这个方法需要传递3个参数，分别是content、from(从哪里开始)和to(到哪里结束)<br>
content参数需要传递为数组形式，推荐先声明数组，然后再传参<br>
重要提示，如果参数含有0，需要加"",否则会报错
### JSsearchBySubscript方法的返回结果？
【和JSsearchByLength方法的返回结果的查看方式是一样的】
